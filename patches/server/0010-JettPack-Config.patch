From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Simon Gardling <titaniumtown@gmail.com>
Date: Thu, 8 Jul 2021 17:33:30 -0400
Subject: [PATCH] JettPack Config


diff --git a/src/main/java/me/titaniumtown/JettPackConfig.java b/src/main/java/me/titaniumtown/JettPackConfig.java
new file mode 100644
index 0000000000000000000000000000000000000000..d81564ed381cff61b978adfa0ab661ac22415a07
--- /dev/null
+++ b/src/main/java/me/titaniumtown/JettPackConfig.java
@@ -0,0 +1,117 @@
+package me.titaniumtown;
+
+import com.google.common.base.Throwables;
+import java.io.File;
+import java.io.IOException;
+import java.lang.reflect.InvocationTargetException;
+import java.lang.reflect.Method;
+import java.lang.reflect.Modifier;
+import java.util.List;
+import java.util.concurrent.TimeUnit;
+import java.util.logging.Level;
+import java.util.regex.Pattern;
+import org.bukkit.Bukkit;
+import org.bukkit.configuration.InvalidConfigurationException;
+import org.bukkit.configuration.file.YamlConfiguration;
+
+public class JettPackConfig {
+
+    public static File CONFIG_FILE;
+    private static final String HEADER = "This is the main configuration file for JettPack.\n"
+                        + "JettPack does some **experimental** and possibly unstable stuff\n"
+                        + "You've been warned\n";
+    /*========================================================================*/
+    public static YamlConfiguration config;
+    public static int version; // since we're remapping sidestreams' configs we need this public
+    public static boolean verbose; // since we're remapping sidestreams' configs we need this public
+    /*========================================================================*/
+
+    public static void init(File configFile) {
+        CONFIG_FILE = configFile;
+        config = new YamlConfiguration();
+        try {
+            config.load(CONFIG_FILE);
+        } catch (IOException ex) {
+        } catch (InvalidConfigurationException ex) {
+            Bukkit.getLogger().log(Level.SEVERE, "Could not load jettpack.yml, please correct your syntax errors", ex);
+            throw Throwables.propagate(ex);
+        }
+        config.options().header(HEADER);
+        config.options().copyDefaults(true);
+        verbose = getBoolean("verbose", false);
+
+        version = getInt("config-version", 1);
+        set("config-version", 1);
+        readConfig(JettPackConfig.class, null);
+    }
+
+    protected static void logError(String s) {
+        Bukkit.getLogger().severe(s);
+    }
+
+    protected static void log(String s) {
+        if (verbose) {
+            Bukkit.getLogger().info(s);
+        }
+    }
+
+    static void readConfig(Class<?> clazz, Object instance) {
+        for (Method method : clazz.getDeclaredMethods()) {
+            if (Modifier.isPrivate(method.getModifiers())) {
+                if (method.getParameterTypes().length == 0 && method.getReturnType() == Void.TYPE) {
+                    try {
+                        method.setAccessible(true);
+                        method.invoke(instance);
+                    } catch (InvocationTargetException ex) {
+                        throw Throwables.propagate(ex.getCause());
+                    } catch (Exception ex) {
+                        Bukkit.getLogger().log(Level.SEVERE, "Error invoking " + method, ex);
+                    }
+                }
+            }
+        }
+
+        try {
+            config.save(CONFIG_FILE);
+        } catch (IOException ex) {
+            Bukkit.getLogger().log(Level.SEVERE, "Could not save " + CONFIG_FILE, ex);
+        }
+    }
+
+    private static final Pattern SPACE = Pattern.compile(" ");
+    private static final Pattern NOT_NUMERIC = Pattern.compile("[^-\\d.]");
+
+
+    private static void set(String path, Object val) {
+        config.set(path, val);
+    }
+
+    private static boolean getBoolean(String path, boolean def) {
+        config.addDefault(path, def);
+        return config.getBoolean(path, config.getBoolean(path));
+    }
+
+    private static double getDouble(String path, double def) {
+        config.addDefault(path, def);
+        return config.getDouble(path, config.getDouble(path));
+    }
+
+    private static float getFloat(String path, float def) {
+        return (float) getDouble(path, (double) def);
+    }
+
+    private static int getInt(String path, int def) {
+        config.addDefault(path, def);
+        return config.getInt(path, config.getInt(path));
+    }
+
+    private static <T> List<T> getList(String path, List<T> def) {
+        config.addDefault(path, def);
+        return (List<T>) config.getList(path, config.getList(path));
+    }
+
+    private static String getString(String path, String def) {
+        config.addDefault(path, def);
+        return config.getString(path, config.getString(path));
+    }
+}
\ No newline at end of file
diff --git a/src/main/java/net/minecraft/server/dedicated/DedicatedServer.java b/src/main/java/net/minecraft/server/dedicated/DedicatedServer.java
index 38a0fb9a7c4ade9cacfd30dffabfea7e6b773981..622b64e47bc221250491f72eabbd674f0c80c81c 100644
--- a/src/main/java/net/minecraft/server/dedicated/DedicatedServer.java
+++ b/src/main/java/net/minecraft/server/dedicated/DedicatedServer.java
@@ -233,6 +233,15 @@ public class DedicatedServer extends MinecraftServer implements ServerInterface
         io.papermc.paper.brigadier.PaperBrigadierProviderImpl.INSTANCE.getClass(); // init PaperBrigadierProvider
         // Paper end
 
+        // Yatopia start
+        try {
+            me.titaniumtown.JettPackConfig.init((java.io.File) options.valueOf("jettpack-settings"));
+        } catch (Exception e) {
+            DedicatedServer.LOGGER.error("Unable to load server configuration", e);
+            return false;
+        }
+        // Yatopia end
+
         this.setPvpAllowed(dedicatedserverproperties.pvp);
         this.setFlightAllowed(dedicatedserverproperties.allowFlight);
         this.setResourcePack(dedicatedserverproperties.resourcePack, this.getPackHash());
diff --git a/src/main/java/org/bukkit/craftbukkit/CraftServer.java b/src/main/java/org/bukkit/craftbukkit/CraftServer.java
index b38bb2a399c3ab867e51b06a39a55d8446f8158c..f69f74297f526cfdef6576b860e99e95057e1bb6 100644
--- a/src/main/java/org/bukkit/craftbukkit/CraftServer.java
+++ b/src/main/java/org/bukkit/craftbukkit/CraftServer.java
@@ -931,6 +931,7 @@ public final class CraftServer implements Server {
 
         org.spigotmc.SpigotConfig.init((File) console.options.valueOf("spigot-settings")); // Spigot
         com.destroystokyo.paper.PaperConfig.init((File) console.options.valueOf("paper-settings")); // Paper
+        me.titaniumtown.JettPackConfig.init((File) console.options.valueOf("jettpack-settings")); // JettPack
         for (ServerLevel world : this.console.getAllLevels()) {
             // world.serverLevelData.setDifficulty(config.difficulty); // Paper - per level difficulty
             world.setSpawnSettings(world.serverLevelData.getDifficulty() != Difficulty.PEACEFUL && config.spawnMonsters, config.spawnAnimals); // Paper - per level difficulty (from MinecraftServer#setDifficulty(ServerLevel, Difficulty, boolean))
diff --git a/src/main/java/org/bukkit/craftbukkit/Main.java b/src/main/java/org/bukkit/craftbukkit/Main.java
index 55bae3efbc630be6d40d415509de4c3e744a5004..bc26c18535e161d4254b644ffbdaad23e95ff631 100644
--- a/src/main/java/org/bukkit/craftbukkit/Main.java
+++ b/src/main/java/org/bukkit/craftbukkit/Main.java
@@ -153,6 +153,14 @@ public class Main {
                         .describedAs("Jar file");
                 // Paper end
 
+                // JettPack start
+                acceptsAll(asList("jettpack", "jettpack-settings"), "File for jettpack settings")
+                        .withRequiredArg()
+                        .ofType(File.class)
+                        .defaultsTo(new File("jettpack.yml"))
+                        .describedAs("Yml file");
+                // JettPack end
+
                 // Paper start
                 acceptsAll(asList("server-name"), "Name of the server")
                         .withRequiredArg()
